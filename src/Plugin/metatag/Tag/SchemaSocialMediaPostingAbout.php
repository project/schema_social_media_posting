<?php

declare(strict_types = 1);

namespace Drupal\schema_social_media_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_social_media_posting_about' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_social_media_posting_about",
 *   label = @Translation("about"),
 *   description = @Translation("Comma separated list of what the social media posting is about, for instance taxonomy terms or categories."),
 *   name = "about",
 *   group = "schema_social_media_posting",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaSocialMediaPostingAbout extends SchemaNameBase {

}
