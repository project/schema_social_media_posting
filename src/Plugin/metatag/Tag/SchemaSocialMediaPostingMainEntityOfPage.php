<?php

declare(strict_types = 1);

namespace Drupal\schema_social_media_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_social_media_posting_main_entity_of_page' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_social_media_posting_main_entity_of_page",
 *   label = @Translation("mainEntityOfPage"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. The canonical URL of the social media posting page. Specify mainEntityOfPage when the social media posting is the primary topic of the social media posting page."),
 *   name = "mainEntityOfPage",
 *   group = "schema_social_media_posting",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "url",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaSocialMediaPostingMainEntityOfPage extends SchemaNameBase {

}
