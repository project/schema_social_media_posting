<?php

declare(strict_types = 1);

namespace Drupal\schema_social_media_posting\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'SocialMediaPosting' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_social_media_posting",
 *   label = @Translation("Schema.org: SocialMediaPosting"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/SocialMediaPosting",
 *   }),
 *   weight = 10
 * )
 */
class SchemaSocialMediaPosting extends SchemaGroupBase {

}
