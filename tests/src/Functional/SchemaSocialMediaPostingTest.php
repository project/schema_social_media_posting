<?php

declare(strict_types = 1);

namespace Drupal\Tests\schema_social_media_posting\Functional;

use Drupal\Tests\schema_metatag\Functional\SchemaMetatagTagsTestBase;

/**
 * Tests that each of the Schema Social Media Posting tags work correctly.
 *
 * @group schema_metatag
 * @group schema_social_media_posting
 */
class SchemaSocialMediaPostingTest extends SchemaMetatagTagsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['schema_social_media_posting'];

  /**
   * {@inheritdoc}
   */
  public $moduleName = 'schema_social_media_posting';

  /**
   * {@inheritdoc}
   */
  public $groupName = 'schema_social_media_posting';

}
