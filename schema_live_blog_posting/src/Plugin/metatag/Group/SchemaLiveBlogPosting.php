<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'LiveBlogPosting' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_live_blog_posting",
 *   label = @Translation("Schema.org: LiveBlogPosting"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/LiveBlogPosting",
 *   }),
 *   weight = 10
 * )
 */
class SchemaLiveBlogPosting extends SchemaGroupBase {

}
