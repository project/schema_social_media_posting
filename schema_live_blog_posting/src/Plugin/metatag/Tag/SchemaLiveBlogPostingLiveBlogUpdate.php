<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * A plugin for the 'schema_live_blog_posting_live_blog_update' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_live_blog_posting_live_blog_update",
 *   label = @Translation("liveBlogUpdate"),
 *   description = @Translation("An update to the LiveBlog."),
 *   name = "liveBlogUpdate",
 *   group = "schema_live_blog_posting",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "blog_posting",
 *   tree_parent = {
 *     "BlogPosting",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaLiveBlogPostingLiveBlogUpdate extends SchemaNameBase {

}
