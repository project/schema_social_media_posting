<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_web_page_speakable' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_live_blog_posting_shared_content",
 *   label = @Translation("sharedContent"),
 *   description = @Translation("A creative work shared as part of this posting."),
 *   name = "sharedContent",
 *   group = "schema_live_blog_posting",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "creative_work",
 *   tree_parent = {
 *     "CreativeWork",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaLiveBlogPostingSharedContent extends SchemaNameBase {

}
