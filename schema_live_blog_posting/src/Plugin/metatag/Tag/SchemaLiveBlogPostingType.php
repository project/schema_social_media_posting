<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_live_blog_posting_description' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_live_blog_posting_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of live blog posting."),
 *   name = "@type",
 *   group = "schema_live_blog_posting",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "LiveBlogPosting",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaLiveBlogPostingType extends SchemaNameBase {

}
