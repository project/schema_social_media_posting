<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'coverageEndTime' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_live_blog_posting_coverage_end_time",
 *   label = @Translation("coverageEndTime"),
 *   description = @Translation("The time when the live blog will stop covering the event."),
 *   name = "coverageEndTime",
 *   group = "schema_live_blog_posting",
 *   weight = 4,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "date_time",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaLiveBlogPostingCoverageEndTime extends SchemaNameBase {

}
