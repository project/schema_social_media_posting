<?php

declare(strict_types = 1);

namespace Drupal\schema_live_blog_posting\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'author' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_live_blog_posting_author",
 *   label = @Translation("author"),
 *   description = @Translation("REQUIRED BY GOOGLE. Author of the live blog posting."),
 *   name = "author",
 *   group = "schema_live_blog_posting",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "organization",
 *   tree_parent = {
 *     "Person",
 *     "Organization",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaLiveBlogPostingAuthor extends SchemaNameBase {

}
